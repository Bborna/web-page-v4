import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  template: `
  <link rel="stylesheet" href="./about.scss">

    <section class="hero is-info is-bold">
      <div class="hero-body">
        <div class="container">
          <h1 class="title">About</h1>
        </div>
      </div>
    </section>

    <section class="section container">
      <h2 class="title ">Released in 2011</h2> 
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia, nam. Pariatur, porro earum! Asperiores vitae eveniet accusantium, modi nam perferendis.</p>
      <h2 class="title mt-3">Successor to Nintendo DS</h2>
      <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ullam ab labore dicta delectus voluptas magni nulla mollitia voluptatibus, perspiciatis illo!</p>
      <h2 class="title mt-3">Backwards compatible with <a href="https://en.wikipedia.org/wiki/Nintendo_DS">DS</a></h2>
      <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Qui aliquid eligendi adipisci, quibusdam dolor aliquam harum magni optio nam? Dolorum consequuntur nam animi, consequatur itaque consectetur quisquam officiis ipsam, nostrum architecto a velit voluptas quo est facere illo, beatae sunt.</p>
      <h2 class="title mt-3">Cartridges:</h2>
      
      <figure class="image p-2">
        <img id="first_picture" class ="my-2" src="../../assets/img/3ds-cartridge-front-back.jpeg">
        <img id="second_picture" class ="my-2" src="../../assets/img/3ds-cartridges-package.jpeg">
      </figure>

      <h2 class="title mt-3">New Circle Pad and more</h2>
      <figure class="image p-2">
        <img id="third_picture" class ="my-2" src="../../assets/img/3ds-button-map.png">
      </figure>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellat quis explicabo amet exercitationem id, aperiam eligendi distinctio nostrum quasi tempore deserunt quos, maiores, itaque sunt quidem sit aliquid expedita iusto consequatur incidunt mollitia soluta nihil laborum nam! Voluptate vero natus, unde laudantium asperiores sequi dolores deleniti recusandae atque dolorum temporibus, nemo, quam rerum eveniet quidem!</p>
      <br>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Distinctio provident aut corrupti! Minima voluptatum obcaecati nobis voluptates dolor blanditiis porro quod adipisci laborum? Tenetur eius ratione expedita? Dolorem ipsa, eveniet odit voluptas nobis reiciendis maxime temporibus inventore quod ad earum laudantium quaerat ex ab,</p>
      <h3 class="title mt-3">Home Menu on Dual Screen</h3>
      <figure class="image p-2">
        <img id="fourth_picture" class ="my-2" src="../../assets/img/3ds-home-menu.jpg">
      </figure>
      <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Asperiores, officia. Quisquam perspiciatis eos aperiam ut rerum facere odit inventore reprehenderit quibusdam! Cum veniam laudantium pariatur? Saepe veniam quisquam aperiam esse corporis modi architecto cupiditate adipisci eos explicabo est quibusdam, tenetur incidunt. Labore porro, aperiam sed assumenda in neque asperiores, quae autem at eos doloremque esse temporibus impedit quos itaque dolor.
      </p>
      <br>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis explicabo magni architecto eaque repellat, porro voluptate omnis non quos distinctio laborum quia earum reiciendis mollitia ab rem alias, recusandae itaque?
      </p>
      <br>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati facere optio rem quis mollitia voluptas tempore facilis, ipsam, et assumenda magni minus sapiente maxime nisi? Voluptatem voluptatum iure vero non, corrupti tenetur. Libero molestiae laborum quaerat, corporis doloremque ipsam quas.
      </p>

      <h5 class="title mt-3">Full Trailer:</h5>
      <figure class="image is-21by9">
        <iframe width="640" height="480" src="https://www.youtube.com/embed/ksgFDNKbdts" frameborder="1" allowfullscreen></iframe>
      </figure>
    </section>
    
  `,
  styles: [
  ]
})
export class AboutComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

}
