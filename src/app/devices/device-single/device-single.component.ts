import { findNode } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, UrlSegment } from '@angular/router';
import { DeviceService } from 'src/app/core/services/device.service';

import { Nintendo3DS } from '../device-list/device-list.component';
import { DeviceListComponent } from '../device-list/device-list.component';

@Component({
  selector: 'app-device-single',
  template: `
  <div class="container">
    <button class=" container button is-warning mt-4 "routerLink="/devices">Back</button>
</div>
  <section class="section">
    <div class="container">
      <div class="card" *ngIf="device">
        <header class="card-header-title">
          {{name}}
        </header>

        <span class="card-content"><img src={{imgUrl}}></span>
        
        <h2 class="card-content">
          {{year}}
        </h2>
          <p class="card-footer p-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta rerum numquam consectetur modi blanditiis nemo alias architecto magnam voluptate a at eveniet quidem nisi, quia eius excepturi illo neque quo.</p>
      </div>
    </div>
  </section>
  <section class="section">
    <div class="container" > <!-- tu *ngIf-->
      <div class="card">    
        <header class="card-header-title">
          Most Popular game:
        </header>
        <span class="card-content ">
          <img src={{gameImgUrl}}>
        </span>
        <p class="card-footer p-2">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ducimus itaque eius excepturi iure alias? Sint facere reiciendis, vel, animi saepe, ipsam optio quasi asperiores quod veritatis ullam tenetur nostrum nobis velit quae inventore dolores atque. Dolor, inventore?</p>
      </div>
    </div>
  </section>
  `,
  styles: [
  ]
})
export class DeviceSingleComponent implements OnInit {

  device :any = []
  imgUrl :string = ""
  year :string = ""
  name : string =""
  description : string =""
  gameImgUrl : string = ""
  constructor(
    private deviceService: DeviceService,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void 
  {
    this.route.params.subscribe(params=>{
      var name = params['name']
      this.device = name
      try {
      switch(name[0])
      {
        case "1":
          this.imgUrl = "../../assets/imgOf3ds/org3ds.jpg"
          this.name = "Nintendo 3DS"
          this.year = "2011"
          this.gameImgUrl = "../../assets/imgOfGames/super-mario-3ds-land.jpg"
          break
        case "2":
          this.imgUrl = "../../assets/imgOf3ds/xl3ds.jpg"
          this.name = "Nintendo 3DS XL"
          this.year = "2012"
          this.gameImgUrl = "../../assets/imgOfGames/mario-kart-7.jpg"
          break
        case "3":
          this.imgUrl = "../../assets/imgOf3ds/org2ds.jpg"
          this.name = "Nintendo 2DS"
          this.year = "2013"
          this.gameImgUrl = "../../assets/imgOfGames/legend_of_zelda.png"
          break
        case "4":
          this.imgUrl = "../../assets/imgOf3ds/new3ds.png"
          this.name = "New Nintendo 3DS"
          this.year = "2015"
          this.gameImgUrl = "../../assets/imgOfGames/xenoblade-chronicles.png"
          break
        case "5":
          this.imgUrl = "../../assets/imgOf3ds/newxl3ds.jpg"
          this.name = "New Nintendo 3DS XL"
          this.year = "2015"
          this.gameImgUrl = "../../assets/imgOfGames/dr-mario-miracle-cure.png"
          break
        case "6":
          this.imgUrl = "../../assets/imgOf3ds/newxl2ds.png"
          this.name = "New Nintendo 2DS XL"
          this.year = "2017"
          this.gameImgUrl = "../../assets/imgOfGames/mario-luigi-superstar-saga.jpg"
          break
      }
    
    
    
    
      }
      catch {}
    });





  }

}
