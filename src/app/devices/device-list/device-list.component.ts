import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DeviceService } from 'src/app/core/services/device.service';

@Component({
  selector: 'app-device-list',
  template: `
  <section class="hero is-info is-bold">
      <div class="hero-body">
        <div class="container">
          <h1 class="title">All types of 3DS</h1>
        </div>
      </div>
    </section>

  <section class="section">
    <div class="container">

      <div class="columns is-multiline" *ngIf="devices">
        <div class="column is-4" *ngFor="let model of n3ds">
          <div class="card">
            <div class="card-content">
              <img src="{{model.picture}}">
              <a routerLink="{{model.id}}">
                <br>{{model.name}}<br>
                {{model.year}}

              </a>
            </div>
          </div>
        </div>
      </div>

    </div>
</section>
  `,
  styles: [
  ]
})


export class DeviceListComponent implements OnInit {

  devices :any = []
  n3ds :Nintendo3DS[]= [new Nintendo3DS("Nintendo 3DS","2011","../../assets/imgOf3ds/org3ds.jpg","1_3ds"),
  new Nintendo3DS("Nintendo 3DS XL","2012","../../assets/imgOf3ds/xl3ds.jpg","2_3dsxl"),
  new Nintendo3DS("Nintendo 2DS","2013","../../assets/imgOf3ds/org2ds.jpg","3_2ds"),
  new Nintendo3DS("New Nintendo 3DS","2015","../../assets/imgOf3ds/new3ds.png","4_new3ds"),
  new Nintendo3DS("New Nintendo 3DS XL","2015","../../assets/imgOf3ds/newxl3ds.jpg","5_new3dsxl"),
  new Nintendo3DS("New Nintendo 2DS XL","2017","../../assets/imgOf3ds/newxl2ds.png","6_new2ds")]

  constructor(private deviceService: DeviceService) { }

  ngOnInit(): void {

    this.devices = this.n3ds
    
  }

}

export class Nintendo3DS
{
  name:string
  year:string
  picture:string
  id:string
  constructor(name:string,yearRelease:string,picture:string,id:string)
  {
    this.name=name
    this.year=yearRelease
    this.picture=picture
    this.id=id
  }
}
