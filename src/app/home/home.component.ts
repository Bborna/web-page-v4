import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
  <section class="hero is-primary is-bold is-fullheight">
  <div class="hero-body container ">
    <h1 class="title">
    </h1>
  </div>
  </section>
  `,
  styles: [`
  .hero {
    background-image: url('../../assets/img/3ds-red-blue.jpeg') !important;
    background-size: cover ;
    background-position: center center;

  }
  `]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
