import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeviceListComponent } from './device-list/device-list.component';
import { DeviceSingleComponent } from './device-single/device-single.component';

const routes: Routes = [
  {
    path: '',
    component:DeviceListComponent,
    pathMatch: 'full'
  },
  {
    path: ':name',
    component:DeviceSingleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevicesRoutingModule { }
