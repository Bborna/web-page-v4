import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <div class="navbar is-warning">
      <div class="navbar-brand mx-3">
        <a class ="navbar-item">
          <img src="assets/img/3ds-logo.png">
        </a>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
