import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
    <div class="navbar is-warning">
      <div class="navbar-brand">
        <a class ="navbar-item mx-3">
          <img src="assets/img/3ds-logo.png">
        </a>
      </div>
      <div class="navbar-menu is-dark is-active">
        <div class="navbar-end is-dark">
          <a class="navbar-item" routerLink="/">Home</a>
          <a class="navbar-item" routerLink="/about">About</a>
          <a class="navbar-item" routerLink="/devices">Devices</a>
        </div>
      </div>
    </div>
    
  `,
  styles: [
  ]
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
